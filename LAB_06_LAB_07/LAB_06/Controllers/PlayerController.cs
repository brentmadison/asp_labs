﻿using LAB_06.Models;
using LAB_06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class PlayerController : Controller
    {
        private ISharedRepository playRepo;
        public int PageSize = 5;

        public PlayerController(ISharedRepository playRepo)
        {
            this.playRepo = playRepo;
        }

        public ViewResult Simple(int page = 1)
            => View(new PlayersListViewModel
            {
                Players = playRepo.Players
                .OrderBy(p => p.PlayerId)
                .Skip((page - 1) * PageSize)
                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = playRepo.Players.Count()
                }
            });
    }
}