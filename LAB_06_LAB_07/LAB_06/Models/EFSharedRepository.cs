using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class EFSharedRepository : ISharedRepository
    {
        private NFLDbContext context;

        public IEnumerable<Team> Teams => context.Teams
                    .Include(s => s.Stadium)
                    .Include(p => p.Players)
                    .ThenInclude(pos => pos.Position);

        public IEnumerable<Stadium> Stadiums => context.Stadiums;

        public IEnumerable<Position> Positions => context.Positions;

        public IEnumerable<Player> Players => context.Players
    .Include(t => t.Team)
    .Include(p => p.Position);

        public EFSharedRepository(NFLDbContext context)
        {
            this.context = context;
        }
    }
}