﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Models.ViewModels
{
    public class PlayersListViewModel
    {
        public IEnumerable<Player> Players { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
