using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class Stadium
    {
        public int StadiumId { get; set; }

        public string Name { get; set; }
        public string Location { get; set; }
        public int MaxCapacity { get; set; }

        public List<Team> Teams { get; set; }
    }
}