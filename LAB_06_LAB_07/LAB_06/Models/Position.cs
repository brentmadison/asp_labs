using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class Position
    {
        public int PositionId { get; set; }

        public string Name { get; set; }
        public int Salary { get; set; }
        public List<Player> Players { get; set; }
    }
}