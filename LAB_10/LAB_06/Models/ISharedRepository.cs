using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public interface ISharedRepository
    {
        IEnumerable<Team> Teams { get; }

        IEnumerable<Stadium> Stadiums { get; }

        IEnumerable<Position> Positions { get; }

        IEnumerable<Player> Players { get; }

        void SavePlayer(Player player);

        Player DeletePlayer(int playerID);

        void SaveTeam(Team player);

        Team DeleteTeam(int playerID);
    }
}