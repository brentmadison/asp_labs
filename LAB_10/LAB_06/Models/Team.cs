using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace LAB_06.Models
{
    public class Team
    {
        public int TeamId { get; set; }

        public string Name { get; set; }
        public string Location { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public Stadium Stadium { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public List<Player> Players { get; set; }
    }
}