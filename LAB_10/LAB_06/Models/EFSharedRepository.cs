using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LAB_06.Models
{
    public class EFSharedRepository : ISharedRepository
    {
        private NFLDbContext context;

        public IEnumerable<Team> Teams => context.Teams
                    .Include(s => s.Stadium)
                    .Include(p => p.Players)
                    .ThenInclude(pos => pos.Position);

        public IEnumerable<Stadium> Stadiums => context.Stadiums;

        public IEnumerable<Position> Positions => context.Positions;

        public IEnumerable<Player> Players => context.Players
    .Include(t => t.Team)
    .Include(p => p.Position);

        public EFSharedRepository(NFLDbContext context)
        {
            this.context = context;
        }

        public void SavePlayer(Player player)
        {
            if (player.PlayerId == 0)
            {
                context.Players.Add(player);
            }
            else
            {
                Player dbEntry = context.Players
                    .FirstOrDefault(p => p.PlayerId == player.PlayerId);
                if (dbEntry != null)
                {
                    dbEntry.FirstName = player.FirstName;
                    dbEntry.LastName = player.LastName;
                }
            }
            context.SaveChanges();
        }

        public Player DeletePlayer(int playerID)
        {
            Player dbEntry = context.Players
                .FirstOrDefault(p => p.PlayerId == playerID);
            if (dbEntry != null)
            {
                context.Players.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public void SaveTeam(Team player)
        {
            if (player.TeamId == 0)
            {
                context.Teams.Add(player);
            }
            else
            {
                Team dbEntry = context.Teams
                    .FirstOrDefault(p => p.TeamId == player.TeamId);
                if (dbEntry != null)
                {
                    dbEntry.Name = player.Name;
                    dbEntry.Location = player.Location;
                }
            }
            context.SaveChanges();
        }

        public Team DeleteTeam(int playerID)
        {
            Team dbEntry = context.Teams
                .FirstOrDefault(p => p.TeamId == playerID);
            if (dbEntry != null)
            {
                context.Teams.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}