using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace LAB_06.Models
{
    public class Player
    {
        public int PlayerId { get; set; }

        [Required(ErrorMessage = "Please enter a first name.")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "Please enter a last name.")]
        public string LastName { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public Team Team { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public Position Position { get; set; }
    }
}