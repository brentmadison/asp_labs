using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace LAB_06.Models
{
    public class EFOrderRepository : IOrderRepository
    {
        private NFLDbContext context;

        public EFOrderRepository(NFLDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Order> Orders => context.Orders
                            .Include(o => o.Lines)
                            .ThenInclude(l => l.Player);

        public void SaveOrder(Order order)
        {
            context.AttachRange(order.Lines.Select(l => l.Player));
            if (order.OrderID == 0)
            {
                context.Orders.Add(order);
            }
            context.SaveChanges();
        }
    }
}