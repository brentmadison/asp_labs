﻿using LAB_06.Infastructure;
using LAB_06.Models;
using LAB_06.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class CartController : Controller
    {
        private ISharedRepository repository;
        private Cart cart;

        public CartController(ISharedRepository repo, Cart cartService)
        {
            repository = repo;
            cart = cartService;
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

        public RedirectToActionResult AddToCart(int playerId, string returnUrl)
        {
            Player player = repository.Players
                .FirstOrDefault(p => p.PlayerId == playerId);

            Console.WriteLine(playerId);
            if (player != null)
            {
                cart.AddItem(player, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult RemoveFromCart(int playerId,
                string returnUrl)
        {
            Player player = repository.Players
                .FirstOrDefault(p => p.PlayerId == playerId);

            if (player != null)
            {
                cart.RemoveLine(player);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}