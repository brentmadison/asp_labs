﻿using LAB_06.Models;
using LAB_06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class PlayerController : Controller
    {
        private ISharedRepository playRepo;
        public int PageSize = 5;

        public PlayerController(ISharedRepository playRepo)
        {
            this.playRepo = playRepo;
        }


        public ViewResult Simple(string teamName, int page = 1)
        {
            return View(new PlayersListViewModel
            {
                Players = playRepo.Players
                .Where(p => teamName == null || p.Team == null || p.Team.Name == teamName)
                .OrderBy(p => p.PlayerId)
                .Skip((page - 1) * PageSize)
                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = teamName == null ?
                        playRepo.Players.Count() :
                        playRepo.Players.Where(e =>
                            e.FirstName == teamName).Count()
                },
                CurrentTeamName = teamName
            });

        }

        public ViewResult PlayerChart()
        {
            ViewBag.Players = Newtonsoft.Json.JsonConvert.SerializeObject(playRepo.Players);
            ViewBag.Positions = Newtonsoft.Json.JsonConvert.SerializeObject(playRepo.Positions);
            return View();
        }


        public ViewResult RealPlayers(string teamName)
        {
            Console.WriteLine(teamName);

            ViewBag.teamName = teamName;
            ViewBag.Teams = Newtonsoft.Json.JsonConvert.SerializeObject(playRepo.Teams);
            return View(playRepo.Teams);
        }
    }
}