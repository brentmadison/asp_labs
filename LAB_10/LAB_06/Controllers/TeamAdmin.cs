﻿using LAB_06.Infastructure;
using LAB_06.Models;
using LAB_06.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class TeamAdmin : Controller
    {
        private ISharedRepository repository;

        public TeamAdmin(ISharedRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index() => View(repository.Teams);

        public ViewResult Edit(int teamId) =>
            View(repository.Teams
                .FirstOrDefault(p => p.TeamId == teamId));

        [HttpPost]
        public IActionResult Edit(Team player)
        {
            if (ModelState.IsValid)
            {
                repository.SaveTeam(player);
                TempData["message"] = $"{player.Name} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                // there is something wrong with the data values
                return View(player);
            }
        }

        public ViewResult Create() => View("Edit", new Team());

        [HttpPost]
        public IActionResult Delete(int playerId)
        {
            Team deletedPlayer = repository.DeleteTeam(playerId);
            if (deletedPlayer != null)
            {
                TempData["message"] = $"{deletedPlayer.Name} was deleted";
            }
            return RedirectToAction("Index");
        }
    }
}