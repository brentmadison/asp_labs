﻿using LAB_06.Infastructure;
using LAB_06.Models;
using LAB_06.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class AdminController : Controller
    {
        private ISharedRepository repository;

        public AdminController(ISharedRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index() => View(repository.Players);

        public ViewResult Edit(int playerId) =>
            View(repository.Players
                .FirstOrDefault(p => p.PlayerId == playerId));

        [HttpPost]
        public IActionResult Edit(Player player)
        {
            if (ModelState.IsValid)
            {
                repository.SavePlayer(player);
                TempData["message"] = $"{player.FirstName} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                // there is something wrong with the data values
                return View(player);
            }
        }

        public ViewResult Create() => View("Edit", new Player());

        [HttpPost]
        public IActionResult Delete(int playerId)
        {
            Player deletedPlayer = repository.DeletePlayer(playerId);
            if (deletedPlayer != null)
            {
                TempData["message"] = $"{deletedPlayer.FirstName} was deleted";
            }
            return RedirectToAction("Index");
        }
    }
}