﻿using LAB_06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Components
{
    public class TeamMenuViewComponent : ViewComponent
    {
        private ISharedRepository repository;

        public TeamMenuViewComponent(ISharedRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedTeamName = RouteData?.Values["teamName"];
            return View(repository.Teams
                .Select(x => x.Name)
                .Distinct()
                .OrderBy(x => x));
        }
    }
}
