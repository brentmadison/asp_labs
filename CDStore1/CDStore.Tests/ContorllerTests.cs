using CDStore.Controllers;
using CDStore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Xunit;

namespace CDStore.Tests
{
    public class ControllerTests
    {
        public class ModelFakeRepo : IRepository
        {
            private List<CDObject> cdLines = new List<CDObject>();

            public ModelFakeRepo()
            {
                var newCDs = new[] {
                new CDObject("Test1", 12, 5.00, DateTime.Parse("9/18/2017"), 1000),
                new CDObject("Test2", 60, 15.00, DateTime.Parse("9/18/2017"), 2000),
                new CDObject("Test3", 24, 25.00, DateTime.Parse("9/18/2017"), 3000),
                new CDObject("Test4", 60, 35.00, DateTime.Parse("9/18/2017"), 4000),
            };
                foreach (CDObject p in newCDs)
                {
                    AddCD(p);
                }
            }

            public IEnumerable<CDObject> CDLines => cdLines;

            public void AddCD(CDObject cd)
            {
                cdLines.Add(cd);
            }
        }

        [Fact]
        public void IndexActionModelComplete()
        {
            var controller = new MainController();
            controller.Repository = new ModelFakeRepo();

            var model = (controller.Index() as ViewResult)?.ViewData.Model
                as IEnumerable<CDObject>;

            Assert.Equal(controller.Repository.CDLines, model,
                Comparer.Get<CDObject>((p1, p2) => p1.Bank == p2.Bank
                    && p1.MaturityValue == p2.MaturityValue));
        }
    }
}