using CDStore.Models;
using System;
using Xunit;

namespace CDStore.Tests
{
    public class ModelTests
    {
        [Fact]
        public void TestMaturityDate()
        {
            //Simple test to confirm calculation of maturity date.

            int TEST_START_YEAR = 2000;
            int TEST_YEARS_TERM = 5;

            CDObject test = new CDObject("Test", (TEST_YEARS_TERM * 12), 5.00, DateTime.Parse("1/1/" + TEST_START_YEAR), 1000);

            Assert.Equal(DateTime.Parse("1/1/" + (TEST_START_YEAR + TEST_YEARS_TERM)), test.MaturityDate);
        }

        [Fact]
        public void TestMaturitValueSimple()
        {
            //Could reuse the formula in model. Using https://www.investor.gov/financial-tools-calculators/calculators/compound-interest-calculator

            //Simple test to confirm calculation of maturity value.
            //SINGLE YEAR = SIMPLE INTEREST

            int TEST_INITIAL_VALUE = 1000;
            int TEST_YEARS_TERM = 1;
            double TEST_RATE = 5.00;

            CDObject test = new CDObject("Test", (TEST_YEARS_TERM * 12), TEST_RATE, DateTime.Parse("1/1/2000"), TEST_INITIAL_VALUE);

            Assert.Equal(1050, test.MaturityValue);
        }

        [Fact]
        public void TestMaturitValueCompound()
        {
            //Could reuse the formula in model. Using https://www.investor.gov/financial-tools-calculators/calculators/compound-interest-calculator

            //Simple test to confirm calculation of maturity value.
            //MULTI YEAR = COMPOUND INTEREST

            int TEST_INITIAL_VALUE = 5000;
            int TEST_YEARS_TERM = 5;
            double TEST_RATE = 10.00;

            CDObject test = new CDObject("Test", (TEST_YEARS_TERM * 12), TEST_RATE, DateTime.Parse("1/1/2000"), TEST_INITIAL_VALUE);

            Assert.Equal(8052.55d, (double)test.MaturityValue, 3);
        }
    }
}