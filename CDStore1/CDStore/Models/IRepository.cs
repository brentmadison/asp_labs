﻿using CDStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDStore.Models
{
    public interface IRepository
    {
        IEnumerable<CDObject> CDLines { get; }

        void AddCD(CDObject obj);
    }
}