﻿using CDStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDStore.Models
{
    public class SimpleRepository : IRepository
    {
        private static SimpleRepository sharedRepository = new SimpleRepository();

        private List<CDObject> cdLines = new List<CDObject>();

        public static SimpleRepository SharedRepository => sharedRepository;

        public SimpleRepository()
        {
            var newCDs = new[] {
                new CDObject("Test1", 12, 5.00, DateTime.Parse("9/18/2017"), 1000),
                new CDObject("Test2", 60, 15.00, DateTime.Parse("9/18/2017"), 2000),
                new CDObject("Test3", 24, 25.00, DateTime.Parse("9/18/2017"), 3000),
                new CDObject("Test4", 60, 35.00, DateTime.Parse("9/18/2017"), 4000),
            };
            foreach (CDObject p in newCDs)
            {
                AddCD(p);
            }
        }

        public IEnumerable<CDObject> CDLines => cdLines;

        public void AddCD(CDObject cd)
        {
            cdLines.Add(cd);
        }
    }
}