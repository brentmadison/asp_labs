﻿using LAB_03.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_03.Controllers
{
    public class HomeController : Controller
    {
        public static Student student = new Student { ID = 1, FullName = "Brent Madison" };

        public ViewResult Index()
        {
            ViewBag.title = "Main Page";
            ViewBag.student = student;

            return View();
        }

        public ViewResult ViewCourse()
        {
            Course[] courseArray = {
                        new Course {ID = 1, Credits = 3.5, Name = "ASP.NET", Description = "ASP.NET With Greg", StartDate = new DateTime(2022, 1, 14) },
                        new Course {ID = 2, Credits = 2, Name = "Advanced Topics", Description = "Advanced topics with Nial", StartDate = new DateTime(2022, 1, 14) },
                        new Course {ID = 3, Credits = 0.5, Name = "Programming", Description = "Programming", StartDate = new DateTime(2022, 1, 15) },
                        new Course {ID = 4, Credits = 3, Name = "Math", Description = "Math", StartDate = new DateTime(2022, 1, 16) },
                    };

            ViewBag.title = "View Courses";
            ViewBag.student = student;

            return View("ViewCourse", courseArray);
        }
    }
}