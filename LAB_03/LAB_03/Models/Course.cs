﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_03.Models
{
    public class Course
    {
        public int ID { get; set; }
        public double Credits { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }

        public Boolean? IsFullTime
        {
            get
            {
                return Credits >= 3;
            }
        }
    }
}