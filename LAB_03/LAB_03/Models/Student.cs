﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_03.Models
{
    public class Student
    {
        public int ID { get; set; }
        public string FullName { get; set; }
    }
}
