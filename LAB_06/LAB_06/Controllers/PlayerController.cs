﻿using LAB_06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class PlayerController : Controller
    {
        private IPlayerRepository playRepo;

        public PlayerController(IPlayerRepository playRepo)
        {
            this.playRepo = playRepo;
        }

        public ViewResult Simple()
        {
            return View(playRepo.Players);
        }
    }
}