﻿using LAB_06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class PositionController : Controller
    {
        private IPositionRepository posRepo;

        public PositionController(IPositionRepository posRepo)
        {
            this.posRepo = posRepo;
        }

        public ViewResult Simple()
        {
            return View(posRepo.Positions);
        }
    }
}