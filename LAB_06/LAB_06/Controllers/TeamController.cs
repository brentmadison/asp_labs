﻿using LAB_06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class TeamController : Controller
    {
        private ITeamRepository teamRepo;

        public TeamController(ITeamRepository teamRepo)
        {
            this.teamRepo = teamRepo;
        }

        public ViewResult Simple()
        {
            return View(teamRepo.Teams);
        }

        public ViewResult Complex()
        {
            return View(teamRepo.Teams);
        }
    }
}