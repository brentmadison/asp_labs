﻿using LAB_06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class StadiumController : Controller
    {
        private IStadiumRepository stadRepo;

        public StadiumController(IStadiumRepository stadRepo)
        {
            this.stadRepo = stadRepo;
        }

        public ViewResult Simple()
        {
            return View(stadRepo.Stadiums);
        }
    }
}