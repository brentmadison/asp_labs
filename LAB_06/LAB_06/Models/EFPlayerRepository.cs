using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class EFPlayerRepository : IPlayerRepository
    {
        private NFLDbContext context;
        public IEnumerable<Player> Players => context.Players
            .Include(t => t.Team)
            .Include(p => p.Position);

        public EFPlayerRepository(NFLDbContext context)
        {
            this.context = context;
        }
    }
}