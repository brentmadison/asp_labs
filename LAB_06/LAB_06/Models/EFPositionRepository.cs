using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class EFPositionRepository : IPositionRepository
    {
        private NFLDbContext context;
        public IEnumerable<Position> Positions => context.Positions;

        public EFPositionRepository(NFLDbContext context)
        {
            this.context = context;
        }
    }
}