using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class EFTeamRepository : ITeamRepository
    {
        private NFLDbContext context;

        public IEnumerable<Team> Teams => context.Teams
                    .Include(s => s.Stadium)
                    .Include(p => p.Players)
                    .ThenInclude(pos => pos.Position);

        public EFTeamRepository(NFLDbContext context)
        {
            this.context = context;
        }
    }
}