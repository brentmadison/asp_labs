using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public interface IStadiumRepository
    {
        IEnumerable<Stadium> Stadiums { get; }
    }
}