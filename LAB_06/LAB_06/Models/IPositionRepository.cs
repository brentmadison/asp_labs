using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public interface IPositionRepository
    {
        IEnumerable<Position> Positions { get; }
    }
}