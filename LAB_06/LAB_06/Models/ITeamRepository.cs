using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public interface ITeamRepository
    {
        IEnumerable<Team> Teams { get; }
    }
}