using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public interface IPlayerRepository
    {
        IEnumerable<Player> Players { get; }
    }
}