using System;

namespace LAB_06.Models
{
    public class Player
    {
        public int PlayerId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Team Team { get; set; }
        public Position Position { get; set; }
    }
}