using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class EFStadiumRepository : IStadiumRepository
    {
        private NFLDbContext context;
        public IEnumerable<Stadium> Stadiums => context.Stadiums;

        public EFStadiumRepository(NFLDbContext context)
        {
            this.context = context;
        }
    }
}