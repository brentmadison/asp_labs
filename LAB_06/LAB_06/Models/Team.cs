using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class Team
    {
        public int TeamId { get; set; }

        public string Name { get; set; }
        public string Location { get; set; }

        public Stadium Stadium { get; set; }

        public List<Player> Players { get; set; }
    }
}