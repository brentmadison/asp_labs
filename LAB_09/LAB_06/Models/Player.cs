using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace LAB_06.Models
{
    public class Player
    {
        public int PlayerId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }


        [JsonIgnore]
        [IgnoreDataMember]
        public Team Team { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public Position Position { get; set; }
    }
}