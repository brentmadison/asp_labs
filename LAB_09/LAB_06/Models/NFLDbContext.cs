using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace LAB_06.Models
{
    public class NFLDbContext : DbContext
    {
        public NFLDbContext(DbContextOptions<NFLDbContext> options) : base(options)
        {
        }

        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Stadium> Stadiums { get; set; }

        public DbSet<Order> Orders { get; set; }
    }
}