﻿using LAB_06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class TeamController : Controller
    {
        private ISharedRepository sharedRepo;

        public TeamController(ISharedRepository sharedRepo)
        {
            this.sharedRepo = sharedRepo;
        }

        public ViewResult Simple()
        {
            return View(sharedRepo.Teams);
        }

        public ViewResult Complex()
        {
            return View(sharedRepo.Teams);
        }
    }
}