﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_05.Models
{
    public class SimpleRepository : IRepository
    {
        private static SimpleRepository sharedRepository = new SimpleRepository();

        private Dictionary<string, Product> products
    = new Dictionary<string, Product>();

        public static SimpleRepository SharedRepository => sharedRepository;

        public SimpleRepository()
        {
            var newProducts = new[] {
                new Product { Name = "Product 1", Price = 200M },
                new Product { Name = "Snickers", Price = 12.41M },
                new Product { Name = "Life jacket", Price = 52.41M },
                new Product { Name = "Boat", Price = 931M }
            };
            foreach (var p in newProducts)
            {
                AddProduct(p);
            }
            products.Add("Error", null);
        }

        public IEnumerable<Product> Products => products.Values;

        public void AddProduct(Product p) => products.Add(p.Name, p);
    }
}