using LAB_05.Models;
using System;
using Xunit;

namespace LAB_05Tests
{
    public class ProductTest
    {
        [Fact]
        public void ChangeName()
        {
            var p = new Product { Name = "Test1", Price = 100M };
            p.Name = "Name change";
            Assert.Equal("Name change", p.Name);
        }

        [Fact]
        public void ChangePrice()
        {
            var p = new Product { Name = "Test1", Price = 100M };
            p.Price = 200M;
            Assert.Equal(200M, p.Price);
        }
    }
}