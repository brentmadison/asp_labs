using LAB_05.Controllers;
using LAB_05.Models;
using LAB_05.Tests;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Xunit;

namespace LAB_05Tests
{
    public class ControllerTests
    {
        public class ModelFakeRepo : IRepository
        {
            public IEnumerable<Product> Products { get; } = new Product[]
            {
                new Product{Name = "P1", Price = 20M},
                new Product{Name = "P2", Price = 340M},
                new Product{Name = "P3", Price = 100M},
            };

            public void AddProduct(Product p)
            {
            }
        }

        [Fact]
        public void IndexActionModelComplete()
        {
            var controller = new HomeController();
            controller.Repository = new ModelFakeRepo();

            var model = (controller.Index() as ViewResult)?.ViewData.Model
                as IEnumerable<Product>;

            Assert.Equal(controller.Repository.Products, model,
                Comparer.Get<Product>((p1, p2) => p1.Name == p2.Name
                    && p1.Price == p2.Price));
        }

        public class ModelFakeRepoUnder50 : IRepository
        {
            public IEnumerable<Product> Products { get; } = new Product[]
            {
                new Product{Name = "P1", Price = 20M},
                new Product{Name = "P2", Price = 30M},
                new Product{Name = "P3", Price = 40M},
            };

            public void AddProduct(Product p)
            {
            }
        }

        [Fact]
        public void IndexActionModelUnder50()
        {
            var controller = new HomeController();
            controller.Repository = new ModelFakeRepoUnder50();

            var model = (controller.Index() as ViewResult)?.ViewData.Model
                as IEnumerable<Product>;

            Assert.Equal(controller.Repository.Products, model,
                Comparer.Get<Product>((p1, p2) => p1.Name == p2.Name
                    && p1.Price == p2.Price));
        }
    }
}