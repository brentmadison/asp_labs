﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_02.Controllers
{
    public class HomeController : Controller
    {
        // GET: HomeController
        /*        public ViewResult Index()
                {
                    return View(new string[] { "C#", "Language", "Features" });
                }*/
        /*        public ViewResult Index()
                {
                    List<string> results = new List<string>();
                    foreach(Product p in Product.GetProducts())
                    {
                        string name = p?.Name;
                        decimal? price = p?.Price;
                        string relatedName = p?.Related?.Name;
                        results.Add(string.Format("name: {0}, Price: {1}, Related: {2}", name, price, relatedName));
                    }

                    return View(results);
                }*/

        /*        public ViewResult Index()
                {
                    List<string> results = new List<string>();
                    foreach (Product p in Product.GetProducts())
                    {
                        string name = p?.Name ?? "<No name>";
                        decimal? price = p?.Price ?? 0;
                        string relatedName = p?.Related?.Name ?? "<None>";
                        results.Add(string.Format("name: {0}, Price: {1}, Related: {2}", name, price, relatedName));
                    }

                    return View(results);
                }*/

        /*        public ViewResult Index()
                {
                    List<string> results = new List<string>();
                    foreach (Product p in Product.GetProducts())
                    {
                        string name = p?.Name ?? "<No name>";
                        decimal? price = p?.Price ?? 0;
                        string relatedName = p?.Related?.Name ?? "<None>";
                        results.Add(string.Format($"Name: {name}, price: {price}, related: {relatedName}"));
                    }

                    return View(results);
                }*/

        /*        public ViewResult Index()
                {
                    string[] names = new string[3];
                    names[0] = "bob";
                    names[1] = "Joe";
                    names[2] = "alice";
                    return View("Index", names);*//**//*

                    return View("Index", new string[] { "Bob, Joe", "Alice" });
                }*/

        /*        public ViewResult Index()
                {
                    Dictionary<string, Product> products = new Dictionary<string, Product> {
                        {
                            "Kayak", new Product
                            {
                                Name = "Kayak",
                                Price = 275M,
                                Category = "Water craft",
                            }},
                                        {
                            "Lifejacket", new Product
                            {
                                Name = "lj",
                                Price = 48M,
                            }}
                    };

                    return View("Index", products.Keys);
                }*/

        /*        public ViewResult Index()
                {
                    Dictionary<string, Product> products = new Dictionary<string, Product>
                    {
                        ["Kayak"] = new Product
                        {
                            Name = "Kayak",
                            Price = 275M,
                            Category = "Water craft",
                        },

                        ["Lifejacket"] = new Product
                        {
                            Name = "lj",
                            Price = 48M,
                        },
                    };

                    return View("Index", products.Keys);
                }*/
        /*        public ViewResult Index()
                {
                    object[] data = new object[] { 275m, 30m, "apple", "orange", 100, 10 };
                    decimal total = 0;
                    for(int i =0;i<data.Length;i++)
                    {
                        if(data[i] is decimal d)
                        {
                            total += d;
                        }
                    }

                    return View("Index", new string[] { $"Total: {total:C2}" });
                }*/

        /*        public ViewResult Index()
                {
                    object[] data = new object[] { 275m, 30m, "apple", "orange", 100, 10 };
                    decimal total = 0;

                    for (int i = 0; i < data.Length; i++)
                    {
                        switch(data[i])
                        {
                            case decimal decimalValue:
                                total += decimalValue;
                                break;

                            case int intValue when intValue > 50:
                                total += intValue;
                                break;
                        }
                    }

                    return View("Index", new string[] { $"Total: {total:C2}" });
                }*/
        /*        public ViewResult Index()
                {
                    ShoppingCart cart = new ShoppingCart { Products = Product.GetProducts() };
                    decimal cartTotal = cart.TotalPrices();

                    return View("Index", new string[] { $"Total: {cartTotal:C2}" });
                }*/

        /*        public ViewResult Index()
                {
                    ShoppingCart cart = new ShoppingCart { Products = Product.GetProducts() };

                    Product[] productArray =
                    {
                        new Product
                                    {
                                        Name = "kayak2",
                                        Price = 275M,
                                    },
                                        new Product
                                    {
                                        Name = "Lifejacket",
                                        Price = 28M,
                                    },
                    };
                    decimal cartTotal = cart.TotalPrices();
                    decimal arrayTotal = productArray.TotalPrices();

                    return View("Index", new string[] { $"Total: {cartTotal:C2}", $"Array: {arrayTotal:C2}" });
                }*/

        /*        public ViewResult Index()
                {
                    ShoppingCart cart = new ShoppingCart { Products = Product.GetProducts() };

                    Product[] productArray =
                    {
                        new Product
                        {
                        Name = "kayak2",
                        Price = 275M,
                        },
                        new Product
                        {
                        Name = "Lifejacket",
                        Price = 28M,
                        },
                        new Product
                        {
                        Name = "soccer",
                        Price = 24.5M,
                        },
                        new Product
                        {
                        Name = "Corner",
                        Price = 55M,
                        },
                    };
                    decimal arrayTotal = productArray.FilterByPrice(20).TotalPrices();
                    decimal nameFilterTotal = productArray.FilterByName('s').TotalPrices();

                    return View("Index", new string[] { $"Array: {arrayTotal:C2}", $"Name: {nameFilterTotal:C2}" });
                }*/

        private bool FilterByPrice(Product p)
        {
            return (p?.Price ?? 0) >= 20;
        }

        /*        public ViewResult Index()
                {
                    ShoppingCart cart = new ShoppingCart { Products = Product.GetProducts() };

                    Product[] productArray =
                    {
                        new Product
                        {
                        Name = "kayak2",
                        Price = 275M,
                        },
                        new Product
                        {
                        Name = "Lifejacket",
                        Price = 28M,
                        },
                        new Product
                        {
                        Name = "soccer",
                        Price = 24.5M,
                        },
                        new Product
                        {
                        Name = "Corner",
                        Price = 55M,
                        },
                    };

                    Func<Product, bool> nameFilter = delegate (Product prod)
                    {
                        return prod?.Name?[0] == 'C';
                    };

                    decimal priceFilter = productArray.Filter(p => (p?.Price ?? 0) >= 20).TotalPrices();
                    decimal nameFilterTotal = productArray.Filter(p => p?.Name?[0] == 'C').TotalPrices();

                    return View("Index", new string[] { $"Array: {priceFilter:C2}", $"Name: {nameFilterTotal:C2}" });
                }*/

        /*        public ViewResult Index() =>
                    View(Product.GetProducts().Select(prop => prop?.Name));*/

        /*        public ViewResult Index()
                {
                    var names = new[] { "Kayak", "Lifejack", "Soccer" };
                    return View(names);
                }*/

        /*        public ViewResult Index()
                {
                    var products = new[] { new {Name = "Kayak", Price = 240M},
                        new {Name = "Lifej", Price = 240M},
                        new {Name = "Soccer", Price = 240M},
                    };
                    return View(products.Select(prop => prop.Name));
                }*/

        /*        public ViewResult Index()
                {
                    var products = new[] { new {Name = "Kayak", Price = 240M},
                        new {Name = "Lifej", Price = 240M},
                        new {Name = "Soccer", Price = 240M},
                    };
                    return View(products.Select(prop => prop.GetType().Name));
                }*/

        /*        public async Task<ViewResult> Index()
                {
                    long? length = await MyAsyncMethods.GetPageLength();
                    return View("Index", new string[] { $"length: {length:C2}" });
                }*/

        /*        public ViewResult Index()
                {
                    var products = new[] { new {Name = "Kayak", Price = 240M},
                                new {Name = "Lifej", Price = 240M},
                                new {Name = "Soccer", Price = 240M},
                            };
                    return View(products.Select(p => $"Name: {p.Name}, Price: {p.Price}"));
                }*/

        public ViewResult Index()
        {
            var products = new[] { new {Name = "Kayak", Price = 240M},
                                        new {Name = "Lifej", Price = 240M},
                                        new {Name = "Soccer", Price = 240M},
                                    };
            return View(products.Select(p => $"Name: {nameof(p.Name)}, Price: {nameof(p.Price)}"));
        }
    }
}