﻿namespace LAB_02
{
    public class Product
    {
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public Product Related { get; set; }
        public string Category { get; set; } = "Watersports";
        public bool InStock { get; }
        public bool NameBeginsWithS => Name?[0] == 'S';

        public Product(bool stock = true)
        {
            InStock = stock;
        }

        public static Product[] GetProducts()
        {
            Product kayak = new Product
            {
                Name = "Kayak",
                Price = 275M,
                Category = "Water craft",
            };
            Product lifejacket = new Product(false)
            {
                Name = "Lifejacket",
                Price = 2375M
            };

            kayak.Related = lifejacket;

            return new Product[] { kayak, lifejacket, null };
        }
    }
}