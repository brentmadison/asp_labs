﻿using System.Net.Http;
using System.Threading.Tasks;

namespace LAB_02
{
    public class MyAsyncMethods
    {
        public static async Task<long?> GetPageLength()
        {
            HttpClient client = new HttpClient();

            var httpMessage = await client.GetAsync("http://www.westerntc.edu/");

            return httpMessage.Content.Headers.ContentLength;
        }
    }
}