﻿using LAB_06.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            /*            services.Configure<CookiePolicyOptions>(options =>
                        {
                            // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                            options.CheckConsentNeeded = context => true;
                            options.MinimumSameSitePolicy = SameSiteMode.None;
                        });*/

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<NFLDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("main")));

            services.AddTransient<ISharedRepository, EFSharedRepository>();

            services.AddMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseStaticFiles();
            app.UseCookiePolicy();/**/
            app.UseSession();

            app.UseMvc(routes =>
            {
                /*                routes.MapRoute(
                                    name: "default",
                                    template: "{controller=Player}/{action=Simple}/{id?}");*/

                routes.MapRoute(
                    name: null,
                    template: "{teamName}/Page{page:int}",
                    defaults: new { controller = "Player", action = "Simple" });

                routes.MapRoute(
                    name: null,
                    template: "Page{page:int}",
                    defaults: new { controller = "Player", action = "Simple", page = 1 });

                routes.MapRoute(
                    name: null,
                    template: "{teamName}",
                    defaults: new { controller = "Player", action = "Simple", page = 1 });

                routes.MapRoute(
                    name: null,
                    template: "",
                    defaults: new { controller = "Player", action = "Simple", page = 1 });

                routes.MapRoute(
                    name: null,
                    template: "{controller}/{action}/{id?}");
            });
        }
    }
}