using System.Collections.Generic;
using System.Linq;
using LAB_06.Models;

namespace LAB_06.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public virtual void AddItem(Player player, int quantity)
        {
            CartLine line = lineCollection
                .Where(p => p.Player.PlayerId == player.PlayerId)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLine
                {
                    Player = player,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public virtual void RemoveLine(Player player) =>
            lineCollection.RemoveAll(l => l.Player.PlayerId == player.PlayerId);

        public virtual decimal ComputeTotalValue() =>
            (decimal)lineCollection.Sum(e => 1.25 * e.Quantity);

        public virtual void Clear() => lineCollection.Clear();

        public virtual IEnumerable<CartLine> Lines => lineCollection;
    }

    public class CartLine
    {
        public int CartLineID { get; set; }
        public Player Player { get; set; }
        public int Quantity { get; set; }
    }
}