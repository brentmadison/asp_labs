﻿using LAB_06.Infastructure;
using LAB_06.Models;
using LAB_06.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_06.Controllers
{
    public class CartController : Controller
    {
        private ISharedRepository repository;

        public CartController(ISharedRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart(),
                ReturnUrl = returnUrl
            });
        }

        public RedirectToActionResult AddToCart(int playerId, string returnUrl)
        {
            Player player = repository.Players
                .FirstOrDefault(p => p.PlayerId == playerId);

            Console.WriteLine(playerId);
            if (player != null)
            {
                Cart cart = GetCart();
                cart.AddItem(player, 1);

                Console.WriteLine(cart.Lines);
                SaveCart(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult RemoveFromCart(int playerId,
                string returnUrl)
        {
            Player player = repository.Players
                .FirstOrDefault(p => p.PlayerId == playerId);

            if (player != null)
            {
                Cart cart = GetCart();
                cart.RemoveLine(player);
                SaveCart(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        private Cart GetCart()
        {
            string dataJson = HttpContext.Session.GetString("Cart");
            Console.WriteLine(dataJson);
            if (dataJson == null)
            {
                return new Cart();
            }
            else
            {
                Cart test = JsonConvert.DeserializeObject<Cart>(dataJson);
                /*
                            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();*/
                return test;
            }
        }

        private void SaveCart(Cart cart)
        {
            string json = JsonConvert.SerializeObject(cart);
            Console.WriteLine(json);

            HttpContext.Session.SetString("Cart", json);

            var currentCart = GetCart();
        }
    }
}