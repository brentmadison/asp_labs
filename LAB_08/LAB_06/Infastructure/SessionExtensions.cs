﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;

namespace LAB_06.Infastructure
{
    public static class SessionExtensions
    {

        public static void SetJson(this ISession session, string key, object value)
        {
            string json = JsonConvert.SerializeObject(value);
            Console.WriteLine(json);
            session.SetString(key, json);

            string sessionJson = session.GetString(key);
        }

        public static T GetJson<T>(this ISession session, string key)
        {
            var sessionData = session.GetString(key);
            return sessionData == null
                ? default(T) : JsonConvert.DeserializeObject<T>(sessionData);
        }
    }
}